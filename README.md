# NumiPython

Cours d'initiation à python réalisé au sein du [master NUMI](http://numi.paris/) de
l'Université Paris Est Marne-la-Vallée.

## Construction des sources

A venir.

## Codes sources Python vus en cours

A venir

## Licence

Sauf mention contraire les sources latex sont sous la Creative Commons
Attribution-ShareAlike 3.0 Unported License.

